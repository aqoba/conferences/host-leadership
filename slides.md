---
author: Thomas Clavier
title: Host Leadership
date: 10 octobre 2023
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg){height=160px} |

<small>

| | |
|-|-|
| <i class="fa fa-mastodon"></i>                               | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas)                |
| <i class="fa fa-building"></i>                               | [@aqoba@pouet.chapril.org](https://pouet.chapril.org/@aqoba)                 |
| <i class="fa fa-envelope"></i>                               | thomas.clavier@aqoba.fr                                                      |
| <i class="fa fa-matrix-org"></i>                             | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org)             |
| <i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i> | +33 6 20 81 81 30                                                            |
| <i class="fa fa-linkedin"></i>                               | [linkedin.com/in/thomasclavier/](https://www.linkedin.com/in/thomasclavier/) |
| <i class="fa fa-globe"></i>                                  | [aqoba.fr](https://aqoba.fr/?utm_source=casp23)                              |

</small>

::: notes

* aqoba
* IUT
* LUMEN
* Logiciel libre
* agilité
* entreprise libérée
:::


# Leadership ? {data-background-image="./includes/full-metal-jacket.jpg" data-state="white50"}

::: notes
Travailler une définition commune du leadership.
:::

# Des managers ?

* Alexandre Ricard
* Michel-Edouard Leclerc
* Xavier Niel
* Alain Afflelou
* Franck Provost
* Jacques-Henri Eyraud

<small>Source : sondage Advent-Capital 2018</small>

# Un bon manager

|     |                 |                                                       |
| --- | --------------- | ----------------------------------------------------- |
| 49% | Décisionnels    | Décider, Organiser, Trouver et résoudre les problèmes |
| 36% | Interpersonnels | Contrôler, Commander, Évaluer                         |
| 15% | Informationnels | Diffuser l'information, Écrire, Écouter, S'informer   |

<small>Source : Calvo-Ryba 2006</small>

::: notes
1590 dirigeants français, dont 23 suivi
:::

# Leaders

* Winston Churchill
* Mahatma Gandhi
* Nelson Mandela
* Abraham Lincoln
* Dalai lama

<small>Source : PwC 2013</small>

::: notes
Vous observez quoi ?

- pas de patron
- pas de femme
- pas de lien de subordination
:::

# Qualités du leader

* Charisme
* Vision
* Management
* Motivation
* Inspiration

<small>Source : Petit & Boulocher 2009</small>

::: notes
3 mots pour définir le leadership
:::

# Leadership

![](./includes/leadership.png)

Une équation à 3 inconnues

::: notes
* Leader et manager c'est pas pareil !
* Un leader avec des qualités qui lui son propre
* Une situation particulière
* Des gens pour suivre
:::

# Leadership ou primauté

> Capacité à organiser un sujet et à être suivi dans l'aventure.

# La métaphore {data-background-image="./includes/wedding.jpg" data-state="white50"}

::: notes
Un constat, tout le monde a déjà été leader, au moins sur l'organisation d'un événement, un mariage, une soirée pizza.

* Tout le monde est capable
* Un ensemble d'automatismes qui une fois analysés peuvent nous donner les moyens d'être leader dans d'autres contextes

:::

# À vous

Pensez à un moment / un événement où vous avez été leader.

::: notes
Je vais vous demander d'enquêter, de creuser vos forces et vos faiblesses, ce que vous avez fait, ce que vous aurriez aimé faire.
:::

# 6 actions

|    |    |    |    |
|--- |--- |--- |--- |
| ![](./includes/initier.png){height=60px}    | Initier        | ![](./includes/inviter.png){height=60px}    | Inviter |
| ![](./includes/creer.png){height=60px}      | Créer l'espace | ![](./includes/garantir.png){height=60px}   | Garantir l'espace |
| ![](./includes/connecter.png){height=60px}  | Connecter      | ![](./includes/participer.png){height=60px} | Participer |

::: notes

:::

# Initier

![](./includes/initier.png)

Écouter ce qui doit venir, avoir une vision

::: notes
* Être à l'écoute des signaux faibles
* Mettre en route
* Avoir une vision, un objectif, un rêve
:::

# À vous

Racontez à votre voisin l'histoire de l'action "initier" de votre expérience.

* Ce que vous avez fait.
* Ce que vous auriez aimé faire.
* Les signaux faibles que vous avez perçu.
* Les éléments qui ont permis la mise en route.
* La vision, l'objectif, le rêve que vous avez eu.

# Inviter

![](./includes/inviter.png)

Faire rêver les gens pour leur donner envie de venir

::: notes
* Proposer de participer à un projet, un sujet, ...
* Reconnaître la valeurs des invités
* Fournir ici des attentes, des recommandations, des informations, ...
* Donner la possibilité de refuser
:::

# À vous

Racontez à votre voisine ou voisin votre histoire d'invitation.

* Ce que vous avez fait.
* Ce que vous auriez aimé faire.
* Comment vous avez fait rêver les inviter ?
* Avez-vous mis en avant les qualités des invités ?
* Avez-vous donné la possibilité de refuser ?
* Avec quoi les invités veulent-ils repartir ?

# Créer l'espace

![](./includes/creer.png)

Organiser un espace propice à l'événement, physique et émotionnel

::: notes
* Un espace propice aux interactions
* Pour que chacun donne le meilleur de lui même
* En fonction de ce que l'on espère qu'il s'y passe
* Être le premier arrivé et le dernier parti
:::

# À vous

Racontez à votre voisin comment vous avez créé l'espace, pourquoi ? Qui était le premier arrivé ? le dernier parti ?

* Ce que vous avez fait.
* Ce que vous auriez aimé faire.

# Garantir l'espace

![](./includes/garantir.png)

Accueillir les gens, garantir les limites et la sécurité de tous au sein de l'espace créé.

::: notes
* Décider qui et quoi fera parti de l'événement, du sujet
* Fermer, ouvrir les frontières
* Mettre en place les routines, les rituels, règles de vie
* Pouvoir d'exclusion temporaire (questions, personnes, ...)
* Clore l'espace à la fin
:::

# À vous

Racontez à votre voisin ou voisine ce que vous avez mis en place pour garantir l'espace.

* Quelles étaient les frontières ?
* Qui était participants ?
* Quel était le sujet, le thème ?
* Avez-vous mis en place des règles ?
* Y a-t-il eu des exclusions ?

# Connecter

![](./includes/connecter.png)

Favoriser les rencontres, faire se rencontrer les gens.

::: notes
Établir les connexions à 3 niveaux :

* l'hôte avec les invités,
* les invités entre eux,
* se connecter au sujets, aux propos.
:::

# À vous

Racontez à votre voisin ou voisine comment vous avez connecté :

* les invités entre eux,
* l'assemblé avec le sujets, les propos.
* vous même avec les invités,
* Ce que vous avez fait.
* Ce que vous auriez aimé faire.

# Participer

![](./includes/participer.png)

Devenir un participant au même titre que tous les invités.

::: notes
* Les invités d'abord
* avec les invités
* relation de parité (non dominante)
* être là au cas où (prévention, crise, etc.)
:::

# À vous

Racontez à votre voisin ou voisine votre façon de participer sans oublier :

* la relation de parité
* la gestion de crise ou des urgences
* Ce que vous avez fait.
* Ce que vous auriez aimé faire.


# 4 lieux
<table>
  <tr>
    <th colspan="2"><center>Derrière</center></th>
    <th colspan="2"><center>Devant</center></th>
  </tr>
  <tr>
    <td><img src="./includes/balcon.png" height=60px></td><td>Sur le balcon</td>
    <td><img src="./includes/projecteurs.png" height=60px></td><td>Sous les projecteurs</td>
  </tr>
  <tr>
    <td><img src="./includes/cuisine.png" height=60px></td><td>Dans la cuisine</td>
    <td><img src="./includes/amis.png" height=60px></td><td>Avec les invités</td>
  </tr>
</table>

::: notes
Toutes les actions peuvent être tenu dans les 4 lieux
:::

# Sous les projecteurs

![](./includes/projecteurs.png)

Être le centre de l'attention

::: notes
* Dans quel situations ?
* Comment obtenir l'attention de tous
* Force ou faiblesse
:::

# À vous

Racontez à votre voisine ou voisin les moments ou vous étiez sous les projecteurs.

* Ce que vous avez fait.
* Ce que vous auriez aimé faire.
* À quoi avez-vous vu que vous étiez sous les projecteurs ?
* Êtes vous à l'aise sous les projecteurs ?

# Avec les invités

![](./includes/amis.png)

Être un parmi les autres

::: notes
* Force ou faiblesse
* Dans quel situations ?
:::

# À vous

Racontez à votre voisin ou voisine les moments ou vous étiez avec les invités.

* Ce que vous avez fait.
* Ce que vous auriez aimé faire.
* À quoi avez-vous vu que vous étiez au milieu des invités à égalité.
* Être au milieu des invités : une force ou une faiblesse ?

# Sur le balcon

![](./includes/balcon.png)

Observer, prendre du recul

::: notes
Observer et identifier les gens qui ont besoin de connections, d'aide ou de logistique.
:::

# À vous

Racontez à votre voisin ou voisine les moments ou vous étiez sur le balcon.

* Ce que vous avez fait.
* Ce que vous avez observé ou entendu.
* Ce que vous auriez aimé faire.
* Quel était les caractéristiques de votre balcon ?

# Dans la cuisine

![](./includes/cuisine.png)

Faire le travail préparatoire, s'isoler.

::: notes
Mais aussi s'isoler, réfléchir, sortir du tumulte pour se recentrer sur soi.
:::

# À vous

Racontez à votre voisine ou voisin les moments ou vous étiez dans la cuisine.

* Ce que vous avez fait.
* Ce que vous auriez aimé faire.
* Quel est votre cuisine ? Votre lieu de ressourcement ?

# Atelier {data-background-image="./includes/workshop.jpg" data-state="white50"}

::: notes
En dehors d'aujourd'hui, comment utiliser le host leadership ?
:::

#

[![](./includes/host-leadership-board.png){style="height: 600px"}](./includes/host-leadership-board.pdf)

# Vous avez été leader

* Quels sont vos points faibles et vos points forts ?
* Comment pouvez vous vous améliorer ?
* Quelles actions vous pouvez entreprendre pour progresser ?

# Vous serez leader

* Connaissant vos points faibles et vos points forts, comment pouvez-vous vous améliorer ?
* Quelles actions vous pouvez entreprendre pour progresser ?
* Avez-vous identifié votre cuisine ? votre balcon ?

# Vous avez observé un leader

* Quels sont ses points faibles et ses points forts ?
* Comment pouvez vous l'aider à s'améliorer ?
* Quelles actions vous pouvez entreprendre pour lui permettre de progresser ?

# Vous allez accompagner un leader

* En tant que manager, accompagner un membre de l'équipe à devenir leader.
* Faire l'état des lieux du leadership d'un collaborateur.

# En résumé ? {data-background-image="./includes/takeaway.jpg" data-state="white90"}

* Leadership ≠ management
* Leadership = organiser un sujet et être suivi
* Une métaphore de l'hôte
* Pistes d'utilisation
    * Pour devenir leader
    * Comme une checklist pour être un leader plus complet
    * Pour se connaitre et identifier ses forces, améliorer ses faiblesses, connaitre ses positions, etc.
    * Pour aider quelqu'un, avec ou sans lui.

::: notes
insister sur :
* la recherche des positions
:::

# Merci
:::: {.columns}
::: {.column width="50%"}
[![](./includes/qrcode.png){height=350}](https://aqoba.gitlab.io/conferences/host-leadership/)
:::
::: {.column width="50%"}

<small>

| |  |
|-|--|
| <i class="fa fa-envelope"></i>                               | thomas.clavier@aqoba.fr                                                                                                                    |
| <i class="fa fa-mastodon"></i>                               | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
| <i class="fa fa-building"></i>                               | [@aqoba@pouet.chapril.org](https://pouet.chapril.org/@aqoba) |
| <i class="fa fa-matrix-org"></i>                             | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org)                                                                           |
| <i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i> | +33 6 20 81 81 30                                                                                                                          |
| <i class="fa fa-linkedin"></i>                               | [linkedin.com/in/thomasclavier/](https://www.linkedin.com/in/thomasclavier/)                                                               |
| <i class="fa fa-globe"></i>                                  | [aqoba.fr](https://aqoba.fr/?utm_source=casp23)                                                                                               |

</small>
:::
::::

Partagez vos expériences

# Pédagogie inversée

# Merci


